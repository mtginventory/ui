import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VersionService {

  constructor(private http: HttpClient) {
  }

  getVersion(): Observable<string> {
    return this.http.get(`/assets/version`, {responseType: 'text'});
  }
}

import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {share} from 'rxjs/operators';
import {InventoryValue} from '../../inventory/model/inventory-value';
import {Inventory} from '../../shared/model/inventory';
import {InventoryCard} from '../../shared/model/inventory-card';
import {PriceService} from './price.service';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  private timestamps$: Observable<Array<number>>;

  constructor(private http: HttpClient, private priceService: PriceService) {

    this.timestamps$ = this.priceService.getTimestamps().pipe(share());
  }

  createInventory(name: string): Observable<Inventory> {
    return this.http.post<Inventory>('/api/inventory', {name});
  }

  getInventories(): Observable<Array<Inventory>> {
    return this.http.get<Array<Inventory>>('/api/inventory');
  }

  getInventory(inventoryId: string, setId?: string): Observable<Array<InventoryCard>> {
    let params = new HttpParams();
    if (setId) {
      params = params.set('set', setId);
    }
    return this.http.get<Array<InventoryCard>>(`/api/inventory/${inventoryId}`, {params});
  }

  addCardToInventory(inventoryId: string, card: { id: string } | { set: string, number: number | string },
                     quantity: number, foil: boolean): Observable<InventoryCard> {
    return this.http.patch<InventoryCard>(`/api/inventory/${inventoryId}/add-card`, {card, quantity, foil});
  }

  removeCardFromInventory(inventoryId: string, card: { id: string } | { set: string, number: number | string },
                          quantity: number, foil: boolean): Observable<InventoryCard> {
    return this.http.patch<InventoryCard>(`/api/inventory/${inventoryId}/remove-card`, {card, quantity, foil});
  }

  setCardQuantity(inventoryId: string, card: { id: string } | { set: string, number: number | string },
                  quantity: number, foil: boolean): Observable<InventoryCard> {
    return this.http.patch<InventoryCard>(`/api/inventory/${inventoryId}/set-card-quantity`, {card, quantity, foil});
  }

  moveCard(fromInventory: string, toInventory: string, card: { id: string } | { set: string, number: number | string },
           foil: boolean): Observable<InventoryCard> {
    return this.http.patch<InventoryCard>(`/api/inventory/move-card`, {
      card,
      foil,
      fromInventory,
      toInventory
    });
  }

  refreshInventory(inventoryId: string): Observable<never> {
    return this.http.patch<never>(`/api/inventory/${inventoryId}/refresh`, {});
  }

  getInventoryValue(inventoryId: string): Observable<Array<InventoryValue>> {
    return this.http.get<Array<InventoryValue>>(`/api/inventory/${inventoryId}/value`);
  }
}

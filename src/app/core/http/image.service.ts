import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {concatMap, switchMap, tap} from 'rxjs/operators';
import {v4 as uuid} from 'uuid';


@Injectable({
  providedIn: 'root'
})
export class ImageService {


  constructor(private http: HttpClient) {
  }

  uploadImages(fileList: FileList): Observable<void> {
    const files: Array<File> = [];
    for (let i = 0; i < fileList.length; i++) {
      const file = fileList.item(i);
      if (file !== null) {
        files.push(file);
      }
    }
    return of(...files).pipe(concatMap(f => this.upload(f)));
  }


  private upload(file: File): Observable<void> {
    let fileName = file.name;
    if (fileName === 'image.jpg') {
      fileName = `${uuid()}.jpg`;
    }
    return this.http.get('/api/image/auth', {
      params: {fileName},
      responseType: 'text'
    }).pipe(
      tap(s => console.dir(s)),
      switchMap(s3Url => this.http.put<void>(s3Url, file))
    );
  }
}

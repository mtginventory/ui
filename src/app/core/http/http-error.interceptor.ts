import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NGXLogger} from 'ngx-logger';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private logger: NGXLogger, private snackBar: MatSnackBar) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          let errorMsg = '';
          if (error.error instanceof ErrorEvent) {
            errorMsg = `Error: ${error.error.message}`;
            this.snackBar.open('There was an error connecting to services', 'Dismiss');
          } else {
            if (error.status === 400 || error.status === 401) {
              this.snackBar.open(error.error,
                'Dismiss');
            } else if (error.status > 500) {
              this.snackBar.open('There was an error processing your request. Please try again later',
                'Dismiss');
            } else {
              this.snackBar.open('An unknown error occurred while processing your request. Please try again later',
                'Dismiss');
            }
            errorMsg = `Error Code: ${error.status},  Message: ${error.message}`;
            if (error.headers.has('x-amzn-trace-id')) {
              const traceId = error.headers.get('x-amzn-trace-id')?.trim().substr(5);
              this.logger.info(`Amazon X-Ray trace https://console.aws.amazon.com/xray/home?region=us-east-1#/traces/${traceId}`);
            }
          }
          this.logger.error(errorMsg);
          return throwError(errorMsg);
        }));
  }
}

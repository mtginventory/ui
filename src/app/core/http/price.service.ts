import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PriceService {

  constructor(private http: HttpClient) {
  }

  getTimestamps(): Observable<Array<number>> {
    return this.http.get<Array<number>>('/api/price/timestamps');
  }
}

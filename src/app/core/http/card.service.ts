import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Card} from '../../shared/model/card';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private http: HttpClient) {
  }

  getAutocomplete(input: string): Observable<Array<Card>> {
    return this.http.get<Array<Card>>('/api/card/autocomplete', {params: {q: input}});
  }

  getCard(set: string, collectorNumber: number): Observable<Card> {
    return this.http.get<Card>(`/api/card/${set}/${collectorNumber}`);
  }

  getSetCards(set: string): Observable<Array<Card>> {
    return this.http.get<Array<Card>>(`/api/card/${set}`);
  }
}

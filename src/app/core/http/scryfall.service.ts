import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ScryfallSets} from '../../direct-add/model/scryfall-sets';

@Injectable({
  providedIn: 'root'
})
export class ScryfallService {

  constructor(private httpClient: HttpClient) {
  }

  getSets(): Observable<ScryfallSets> {
    return this.httpClient.get<ScryfallSets>('https://api.scryfall.com/sets');
  }

}

export interface Inventory {
  id: string;
  name: string;
  createTime: string;
  sets: Array<string>;
  valueTimestamps: Array<number>;
  values: Array<number>;
}

import {Card} from './card';

export interface InventoryCard extends Card {
  quantity: number;
  foilQuantity: number;
  inventoryId: string;
  cardId: string;
}

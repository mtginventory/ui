export interface Card {
  name: string;
  id: string;
  set: string;
  setName: string;
  collectorNumber: string;
  manaCost: string;
  cmc: number;
  imageUris: {
    artCrop: string,
    large: string,
    normal: string,
    small: string,
    png: string
  };
  prices: {
    usd: string,
    eur: string,
    tix: string,
    usdFoil: string
  };
  rarity: string;
}

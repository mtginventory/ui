import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'mtgi-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  readonly cardSearchDescription = 'Quickly add cards to your collections by searching on the card name or set/collection number';
  readonly directAddDescription = 'Rapid fire add cards to your collection using the set and collector numbers. Keep track of what you\'ve added and easily undo any mistakes you\'ve made';
  readonly inventoryDescription = 'Manage your collection inventory and check your set completion';

  constructor() {
  }

  ngOnInit(): void {
  }
}

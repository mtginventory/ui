import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ServiceWorkerModule} from '@angular/service-worker';
import {AuthHttpInterceptor, AuthModule} from '@auth0/auth0-angular';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';

import {CoreModule} from 'src/app/core/core.module';
import {environment} from '../environments/environment';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    CoreModule,
    MatToolbarModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    BrowserAnimationsModule,
    HttpClientModule,
    MatButtonModule,
    LoggerModule.forRoot({level: NgxLoggerLevel.TRACE}),
    MatMenuModule,
    AuthModule.forRoot({
      domain: 'dev-oj0ok9xc.us.auth0.com',
      clientId: 'guRDFEecu7aeUrBYsZdM3FYKSlCaQSGK',
      audience: 'https://api.mtginventory.com',
      httpInterceptor: {
        allowedList: [
          {uri: '/api/*'}
        ]
      }
    })

  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

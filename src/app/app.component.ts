import {Component} from '@angular/core';
import {AuthService} from '@auth0/auth0-angular';
import {Observable} from 'rxjs';
import {VersionService} from './core/http/version.service';

@Component({
  selector: 'mtgi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  authenticated$: Observable<boolean>;
  version = 'Version not loaded';

  constructor(private auth: AuthService, private versionService: VersionService) {
    this.authenticated$ = auth.isAuthenticated$;
    versionService.getVersion().subscribe(v => this.version = v);
  }

  loginClick(): void {
    this.auth.loginWithRedirect();
  }
}

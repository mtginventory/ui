import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', loadChildren: () => import('./landing/landing.module').then(m => m.LandingModule)},
  {path: 'search', loadChildren: () => import('./search/search.module').then(m => m.SearchModule)},
  {
    path: 'image-upload',
    loadChildren: () => import('./image-upload/image-upload.module').then(m => m.ImageUploadModule)
  },
  {path: 'debug', loadChildren: () => import('./debug/debug.module').then(m => m.DebugModule)},
  {path: 'direct-add', loadChildren: () => import('./direct-add/direct-add.module').then(m => m.DirectAddModule)},
  {
    path: 'inventory', loadChildren: () => import('./inventory/inventory.module').then(m => m.InventoryModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

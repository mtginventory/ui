import {Component, OnInit} from '@angular/core';
import {ImageService} from '../../core/http/image.service';

@Component({
  selector: 'mtgi-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss']
})
export class ImageUploadComponent implements OnInit {

  constructor(private imageService: ImageService) {
  }

  ngOnInit(): void {
  }

  uploadImages(event: Event): void {
    const target = event.target as HTMLInputElement;
    const files = target?.files;
    if (files !== null) {
      this.imageService.uploadImages(files).subscribe();
    }
  }

}

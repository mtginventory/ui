import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ImageUploadComponent} from './image-upload/image-upload.component';

const routes: Routes = [
  {path: '', component: ImageUploadComponent}
];

@NgModule({
  declarations: [ImageUploadComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ImageUploadModule {
}

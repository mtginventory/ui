import {Component, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Observable, of} from 'rxjs';
import {concatMap, tap} from 'rxjs/operators';
import {InventoryService} from '../../../core/http/inventory.service';
import {Inventory} from '../../../shared/model/inventory';
import {InventoryCard} from '../../../shared/model/inventory-card';

@Component({
  selector: 'mtgi-move-cards-dialog',
  templateUrl: './move-cards-dialog.component.html',
  styleUrls: ['./move-cards-dialog.component.scss']
})
export class MoveCardsDialogComponent {

  inventories$: Observable<Array<Inventory>>;
  selectedInventory = new FormControl(undefined, Validators.required);
  isFoil = new FormControl(false);
  completedCount = 0;
  totalCount: number;
  moving = false;

  constructor(private dialogRef: MatDialogRef<MoveCardsDialogComponent>, private inventoryService: InventoryService,
              @Inject(MAT_DIALOG_DATA) private cards: Array<InventoryCard>
  ) {
    this.inventories$ = inventoryService.getInventories();
    this.totalCount = cards.length;
  }

  close(): void {
    this.dialogRef.close();
  }

  startMove(): void {
    this.moving = true;
    this.isFoil.disable();
    this.selectedInventory.disable();
    of(...this.cards).pipe(
      concatMap(c => this.inventoryService.moveCard(c.inventoryId, this.selectedInventory.value.id, {id: c.cardId}, this.isFoil.value)),
      tap(() => this.completedCount++)
    ).subscribe();
  }
}

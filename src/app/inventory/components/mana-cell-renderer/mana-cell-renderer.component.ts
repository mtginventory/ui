import {Component, OnInit} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {ICellRendererParams} from 'ag-grid-community';

@Component({
  selector: 'mtgi-mana-cell-renderer',
  templateUrl: './mana-cell-renderer.component.html',
  styleUrls: ['./mana-cell-renderer.component.scss']
})
export class ManaCellRendererComponent implements OnInit, ICellRendererAngularComp {

  symbols: Array<string> = [];

  constructor() {
  }

  static parseSymbols(manaString: string): Array<string> {
    if (!manaString) {
      return [];
    }
    return manaString.replace(/{([A-Z0-9/]+)}/g, '$1,').slice(0, -1).split(',');
  }

  ngOnInit(): void {
  }


  refresh(params: ICellRendererParams): boolean {
    this.symbols = ManaCellRendererComponent.parseSymbols(params.value);
    return true;
  }

  agInit(params: ICellRendererParams): void {
    this.symbols = ManaCellRendererComponent.parseSymbols(params.value);
  }

}

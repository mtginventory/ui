import {DecimalPipe} from '@angular/common';
import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {GridApi, ValueGetterParams} from 'ag-grid-community';
import {InventoryService} from '../../../core/http/inventory.service';
import {InventoryCard} from '../../../shared/model/inventory-card';
import {InventorySummary} from '../../model/inventory-summary';
import {GridNumericEditorComponent} from '../grid-numeric-editor/grid-numeric-editor.component';
import {GridTooltipComponent} from '../grid-tool-tip/grid-tooltip.component';
import {ManaCellRendererComponent} from '../mana-cell-renderer/mana-cell-renderer.component';
import {MoveCardsDialogComponent} from '../move-cards-dialog/move-cards-dialog.component';

@Component({
  selector: 'mtgi-inventory-grid',
  templateUrl: './inventory-grid.component.html',
  styleUrls: ['./inventory-grid.component.scss']
})
export class InventoryGridComponent implements OnInit, OnChanges {

  columnDefs = [
    {
      checkboxSelection: true,
      minWidth: 38,
      width: 38,
      filter: false,
      sortable: false,
      flex: 0,
      headerCheckboxSelection: true
    },
    {
      headerName: 'Card Details',
      children: [
        {headerName: 'Card', field: 'name', tooltipField: 'name', minWidth: 200},
        {headerName: 'Type', field: 'typeLine', minWidth: 200},
        {headerName: 'Mana', field: 'manaCost', cellRenderer: 'manaCellRenderer'},
        {headerName: 'Rarity', field: 'rarity'},
        {headerName: 'EDHREC Rank', field: 'edhrecRank', minWidth: 150, filter: 'agNumberColumnFilter'},
        {headerName: 'Set ID', field: 'set', columnGroupShow: 'open'},
        {headerName: 'Set', field: 'setName', columnGroupShow: 'open'},
        {
          headerName: 'Collector Number',
          columnGroupShow: 'open',
          valueGetter: (p: ValueGetterParams) => parseInt(p.data.collectorNumber, 10)
        },
        {headerName: 'Inventory', field: 'inventoryId', columnGroupShow: 'open'}
      ]
    },
    {
      headerName: 'Inventory Details', children: [
        {
          headerName: 'Total Value',
          valueGetter: (p: ValueGetterParams) => InventoryGridComponent.totalValueGetter(p.data),
          valueFormatter: this.currencyFormatter.bind(this),
          filter: 'agNumberColumnFilter',
          columnGroupShow: 'closed',
          minWidth: 100
        },
        {
          headerName: 'Total Quantity',
          valueGetter: (p: ValueGetterParams) => p.data.quantity + p.data.foilQuantity,
          filter: 'agNumberColumnFilter',
          columnGroupShow: 'closed',
          minWidth: 140
        },
        {
          headerName: 'Regular Quantity',
          field: 'quantity',
          filter: 'agNumberColumnFilter',
          columnGroupShow: 'open',
          editable: true,
          cellEditor: 'numericEditor'
        },
        {
          headerName: 'Regular Price',
          field: 'prices.usd',
          valueFormatter: this.currencyFormatter.bind(this),
          filter: 'agNumberColumnFilter',
          columnGroupShow: 'open'
        },
        {
          headerName: 'Foil Quantity',
          field: 'foilQuantity',
          filter: 'agNumberColumnFilter',
          columnGroupShow: 'open',
          cellEditor: 'numericEditor',
          editable: true
        },
        {
          headerName: 'Foil Price',
          field: 'prices.usdFoil',
          valueFormatter: this.currencyFormatter.bind(this),
          filter: 'agNumberColumnFilter',
          columnGroupShow: 'open'
        }

      ]
    }
  ];
  defaultColDef = {
    sortable: true,
    flex: 1,
    minWidth: 140,
    filter: true,
    resizable: true,
    tooltipComponent: 'customTooltip'
  };
  tooltipShowDelay = 0;
  frameworkComponents = {
    manaCellRenderer: ManaCellRendererComponent,
    customTooltip: GridTooltipComponent,
    numericEditor: GridNumericEditorComponent
  };
  summary: InventorySummary = {cardCount: 0, totalValue: 0};
  @Input()
  rows: Array<InventoryCard> = [];
  @Input()
  loading = false;
  private gridApi: GridApi | undefined;
  private numberPipe = new DecimalPipe('en');

  constructor(private inventoryService: InventoryService, private matDialog: MatDialog) {
  }

  private static totalValueGetter(inventoryCard: InventoryCard): number {
    return (inventoryCard.quantity || 0) * (Number(inventoryCard.prices.usd) || 0)
      + (inventoryCard.foilQuantity || 0) * (Number(inventoryCard.prices.usdFoil));
  }

  ngOnInit(): void {
  }

  onGridReady(params: { api: GridApi }): void {
    this.gridApi = params.api;
  }

  onCellChanged(params: { oldValue: number, newValue: number, colDef: { field: string }, data: InventoryCard }): void {
    const {colDef, newValue, data} = params;
    const {field} = colDef;
    this.gridApi?.showLoadingOverlay();
    this.inventoryService.setCardQuantity(data.inventoryId, {id: data.cardId}, newValue, field === 'foilQuantity')
      .subscribe(() => this.gridApi?.hideOverlay(), () => this.gridApi?.hideOverlay());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.gridApi) {
      if (this.loading) {
        this.gridApi.showLoadingOverlay();
      } else {
        this.gridApi.hideOverlay();
      }
    }
    this.summary.cardCount = this.rows.reduce((v, t) => v + t.quantity + t.foilQuantity, 0);
    this.summary.totalValue = this.rows.reduce((v, t) => v + InventoryGridComponent.totalValueGetter(t), 0);
  }

  moveSelectedCards(): void {
    this.matDialog.open(MoveCardsDialogComponent, {data: this.gridApi?.getSelectedNodes().map(r => r.data)});
  }

  exportCardKingdomCsv(): void {
    const rows: Array<InventoryCard> = this.gridApi?.getSelectedRows() || [];
    let csvData = '';
    let rowCount = 0;
    for (const row of rows) {
      if (row.quantity > 0 && rowCount < 500) {
        csvData += `"${row.name}",${row.setName},0,${row.quantity}\n`;
        rowCount++;
      }
      if (row.foilQuantity > 0 && rowCount < 500) {
        csvData += `"${row.name}",${row.setName},1,${row.foilQuantity}\n`;
        rowCount++;
      }
    }
    const blob = new Blob([csvData], {type: 'text/csv;charset=utf-8;'});
    const link = document.createElement('a');
    const url = URL.createObjectURL(blob);
    link.setAttribute('href', url);
    link.setAttribute('download', 'card_kingdom_card_list.csv');
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  private currencyFormatter(params: { value: string }): string {
    const value = Number(params.value);
    const formattedValue = this.numberPipe.transform(value, '1.2-2');
    return `\$${formattedValue}`;
  }


}

import {Component} from '@angular/core';
import {ITooltipAngularComp} from 'ag-grid-angular';
import {IAfterGuiAttachedParams, ITooltipParams} from 'ag-grid-community';
import {InventoryCard} from '../../../shared/model/inventory-card';

@Component({
  selector: 'mtgi-grid-tool-tip',
  templateUrl: './grid-tooltip.component.html',
  styleUrls: ['./grid-tooltip.component.scss']
})
export class GridTooltipComponent implements ITooltipAngularComp {

  imageUri: string | undefined;

  constructor() {
  }

  afterGuiAttached(params?: IAfterGuiAttachedParams): void {
  }

  agInit(params: ITooltipParams): void {
    const data: InventoryCard = params.api.getDisplayedRowAtIndex(params.rowIndex).data;
    this.imageUri = data.imageUris?.normal;
  }


}

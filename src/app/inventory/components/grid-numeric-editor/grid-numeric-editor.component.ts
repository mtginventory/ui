import {Component, ElementRef, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ICellEditorAngularComp} from 'ag-grid-angular';
import {IAfterGuiAttachedParams, ICellEditorParams} from 'ag-grid-community';

@Component({
  selector: 'mtgi-grid-numeric-editor',
  templateUrl: './grid-numeric-editor.component.html',
  styleUrls: ['./grid-numeric-editor.component.scss']
})
export class GridNumericEditorComponent implements ICellEditorAngularComp {

  params?: ICellEditorParams;
  valueControl = new FormControl();
  @ViewChild('input')
  inputElement?: ElementRef;

  constructor() {
  }

  afterGuiAttached(params?: IAfterGuiAttachedParams): void {
  }

  agInit(params: ICellEditorParams): void {
    this.params = params;
    this.valueControl.setValue(params.value);
    this.focusIn();
  }

  focusIn(): void {
    setTimeout(() => this.inputElement?.nativeElement.focus());
  }

  focusOut(): void {
    setTimeout(() => this.inputElement?.nativeElement.blur());
  }

  getValue(): any {
    return this.valueControl.value;
  }


}

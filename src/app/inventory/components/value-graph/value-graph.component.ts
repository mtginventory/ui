import {DatePipe} from '@angular/common';
import {
  Component,
  DoCheck,
  EventEmitter,
  Input,
  IterableDiffer,
  IterableDiffers,
  Output,
  ViewChild
} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {BaseChartDirective, Label} from 'ng2-charts';
import {concat, interval, Observable, of} from 'rxjs';
import {concatMap, filter, map, switchMapTo, tap, toArray} from 'rxjs/operators';
import {InventoryService} from '../../../core/http/inventory.service';
import {PriceService} from '../../../core/http/price.service';
import {Inventory} from '../../../shared/model/inventory';
import {InventoryValue} from '../../model/inventory-value';

@Component({
  selector: 'mtgi-value-graph',
  templateUrl: './value-graph.component.html',
  styleUrls: ['./value-graph.component.scss']
})
export class ValueGraphComponent implements DoCheck {

  @Input()
  loading = false;
  @Input()
  active = false;
  @Input()
  inventories?: Array<Inventory>;
  @Output() refreshSelectedInventories = new EventEmitter();
  @ViewChild(BaseChartDirective)
  chart?: BaseChartDirective;

  private iterableDiffer: IterableDiffer<Inventory>;

  barChartLabels$: Observable<Array<Label>>;
  timestamps: Array<number> = [];
  chartType: ChartType = 'line';
  barChartData$: Observable<Array<ChartDataSets>> = of();
  datePipe = new DatePipe('en-US');
  chartOptions: ChartOptions = {
    spanGaps: true,
    scales: {
      yAxes: [{
        ticks: {
          callback: (value) => {
            return `$${(value as number).toFixed(2)}`;
          }
        }
      }]
    },
    tooltips: {
      callbacks: {
        label: (tooltipItem: Chart.ChartTooltipItem, data: Chart.ChartData) => {
          return `$${(tooltipItem.yLabel as number).toFixed(2)}`;
        }
      }
    }
  };


  constructor(private iterableDiffers: IterableDiffers, private priceService: PriceService, private inventoryService: InventoryService) {
    this.iterableDiffer = iterableDiffers.find([]).create();
    this.barChartLabels$ = priceService.getTimestamps().pipe(
      tap(ts => this.timestamps = ts),
      map(ts => ts.map(t => this.datePipe.transform(t, 'short')) as Array<string>)
    );
  }

  ngDoCheck(): void {
    if (this.inventories) {
      const changes = this.iterableDiffer.diff(this.inventories);
      if (changes) {
        this.barChartData$ = concat(of(0), interval(60000))
          .pipe(
            filter(i => i === 0 || this.active),
            switchMapTo(this.getInventoryData(this.inventories || []))
          );
      }
    }
  }

  private getInventoryData(inventories: Array<Inventory>): Observable<Array<ChartDataSets>> {
    return of(...inventories).pipe(
      concatMap(i => this.inventoryService.getInventoryValue(i.id)),
      filter(values => values.length > 0),
      map((values: Array<InventoryValue>) => {
        const inventory = inventories.find(i => i.id === values[0].inventoryId);
        const valuesByTimestamp = values.reduce((o: { [key: number]: number }, v) => {
          o[v.timestamp] = v.usd;
          return o;
        }, {});
        return {label: inventory?.name, data: this.timestamps.map(t => valuesByTimestamp[t] || undefined)};
      }),
      toArray());
  }

  refreshValueGraph(): void {
    this.refreshSelectedInventories.emit();
  }
}

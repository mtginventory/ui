import {Component, EventEmitter, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {combineLatest, Observable, of} from 'rxjs';
import {concatMap, last, map, mergeMap, reduce, share, shareReplay, startWith, switchMap, tap} from 'rxjs/operators';
import {InventoryService} from '../../../core/http/inventory.service';
import {Inventory} from '../../../shared/model/inventory';
import {InventoryCard} from '../../../shared/model/inventory-card';

@Component({
  selector: 'mtgi-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {

  inventoryList$: Observable<Array<Inventory>>;
  selectedIndex$: Observable<number>;
  setControl = new FormControl();
  loading = false;
  rows$: Observable<Array<InventoryCard>>;
  sets$: Observable<Array<string>>;
  selectedInventories: Array<Inventory> = [];
  selectedInventories$: EventEmitter<Array<Inventory>> = new EventEmitter<Array<Inventory>>();
  cache: Map<string, Array<InventoryCard>> = new Map<string, Array<InventoryCard>>();

  constructor(private inventoryService: InventoryService, private activatedRoute: ActivatedRoute, private router: Router) {
    const inventories = this.inventoryService.getInventories().pipe(share());
    this.inventoryList$ = inventories;
    inventories.subscribe((i: Array<Inventory>) => this.setInventoryChecked(true, i[0]));
    this.rows$ = combineLatest([this.selectedInventories$.asObservable(), this.setControl.valueChanges.pipe(
      startWith(undefined))]).pipe(
      tap(([i, s]) => this.loading = true),
      switchMap(([i, s]: [Array<Inventory>, string]) => this.getCardsForInventories(i, s)),
      tap(i => this.loading = false),
      shareReplay(1)
    );
    this.sets$ = this.selectedInventories$.pipe(
      map((i) => i.map(x => x.sets).reduce((a, s) => {
        a.push(...s);
        return a;
      }, [])),
      map(a => Array.from(new Set(a))),
      map(a => a.sort())
    );
    this.selectedIndex$ = activatedRoute.queryParamMap.pipe(
      map(p => p.get('tabIndex') || ''),
      map(s => parseInt(s, 10))
    )
    ;
  }

  private static generateCacheId(inventoryId: string, set: string): string {
    return `${inventoryId}|${set}`;
  }

  ngOnInit(): void {
  }

  setInventoryChecked(checked: boolean, inventory: Inventory): void {
    if (!checked) {
      this.selectedInventories.splice(this.selectedInventories.indexOf(inventory), 1);
      const deleteKeys = Array.from(this.cache.keys()).filter(k => k.startsWith(inventory.id));

      deleteKeys.forEach(k => this.cache.delete(k));
    } else {
      this.selectedInventories.push(inventory);
    }
    this.selectedInventories$.emit(this.selectedInventories);
  }

  private getCardsForInventories(inventories: Array<Inventory>, set: string | undefined): Observable<Array<InventoryCard>> {
    return of(...inventories)
      .pipe(
        mergeMap((inventory) => this.getCardsForInventory(inventory.id, set)),
        reduce((cards: Array<InventoryCard>, ic: Array<InventoryCard>) => {
          cards.push(...ic);
          return cards;
        }, [])
      );
  }

  private getCardsForInventory(inventoryId: string, set?: string): Observable<Array<InventoryCard>> {
    const cacheId = InventoryComponent.generateCacheId(inventoryId, set || '');
    if (this.cache.has(cacheId)) {
      // tslint:disable-next-line:ban-ts-ignore
      // @ts-ignore
      return of(this.cache.get(cacheId));
    }
    return this.inventoryService.getInventory(inventoryId, set)
      .pipe(tap(i => this.cache.set(cacheId, i)));
  }

  setSelectedTabIndex(tabIndex: number): void {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {tabIndex},
      queryParamsHandling: 'merge'
    }).then();
  }

  refreshSelectedInventories(): void {
    this.loading = true;
    of(...this.selectedInventories).pipe(
      concatMap(inventory => this.inventoryService.refreshInventory(inventory.id)),
      last(),
      concatMap(() => this.inventoryService.getInventories())
    ).subscribe((inventories) => {
      this.inventoryList$ = of(inventories);
      for (const item of this.selectedInventories) {
        const updatedInventory = inventories.find(i => i.id === item.id);
        if (updatedInventory === undefined) {
          console.warn('Unable to find updated inventory for ', item);
        } else {
          this.selectedInventories[this.selectedInventories.indexOf(item)] = updatedInventory;
        }
      }
      this.selectedInventories$.emit(this.selectedInventories);
    }, () => { this.loading=false;
    }, () => this.loading = false);
  }
}

import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {shareReplay} from 'rxjs/operators';
import {CardService} from '../../../core/http/card.service';
import {ScryfallService} from '../../../core/http/scryfall.service';
import {ScryfallSets} from '../../../direct-add/model/scryfall-sets';
import {InventoryCard} from '../../../shared/model/inventory-card';

@Component({
  selector: 'mtgi-set-grid',
  templateUrl: './set-completion.component.html',
  styleUrls: ['./set-completion.component.scss']
})
export class SetCompletionComponent implements OnInit, OnChanges {

  @Input()
  rows: Array<InventoryCard> = [];
  @Input()
  loading = false;
  @Input()
  set: string | undefined;
  setCards: Array<InventoryCard> = [];
  readonly rarities = ['common', 'uncommon', 'rare', 'mythic'];
  cardCount = 0;
  ownedCardCount = 0;
  private sets$: Observable<ScryfallSets>;
  private cardSubscription: Subscription | undefined;

  constructor(private scryfallService: ScryfallService, private cardService: CardService) {
    this.sets$ = scryfallService.getSets().pipe(shareReplay(1));
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.cardSubscription) {
      this.cardSubscription.unsubscribe();
    }
    this.sets$.subscribe(sets => {
      const scryfallSet = sets.data.find(s => s.code.toLowerCase() === this.set?.toLowerCase());
      if (scryfallSet) {
        this.setCards = Array(scryfallSet.card_count).fill({});
        const setCards = this.rows.filter(r => r.set.toLowerCase() === this.set?.toLowerCase());
        setCards.forEach(c => this.setCards[Number(c.collectorNumber) - 1] = c);
        this.ownedCardCount = setCards.length;
        this.cardCount = scryfallSet.card_count;
        this.cardSubscription = this.cardService.getSetCards(scryfallSet.code).subscribe(cards => {
          for (const card of cards) {
            const index = Number(card.collectorNumber) - 1;
            if (this.setCards[index]?.rarity === undefined) {
              this.setCards[index] = {...card, quantity: 0, foilQuantity: 0, cardId: card.id, inventoryId: ''};
            } else {
              this.setCards[index].imageUris = card.imageUris;
            }
          }
        });
      }
    });
  }

}

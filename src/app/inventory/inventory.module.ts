import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTooltipModule} from '@angular/material/tooltip';
import {RouterModule} from '@angular/router';
import {NgbPopoverModule} from '@ng-bootstrap/ng-bootstrap';
import {AgGridModule} from 'ag-grid-angular';
import {GridNumericEditorComponent} from './components/grid-numeric-editor/grid-numeric-editor.component';
import {GridTooltipComponent} from './components/grid-tool-tip/grid-tooltip.component';
import {InventoryGridComponent} from './components/inventory-grid/inventory-grid.component';
import {InventoryComponent} from './components/inventory/inventory.component';
import {ManaCellRendererComponent} from './components/mana-cell-renderer/mana-cell-renderer.component';
import {MoveCardsDialogComponent} from './components/move-cards-dialog/move-cards-dialog.component';
import {SetCompletionComponent} from './components/set-completion/set-completion.component';
import {ValueGraphComponent} from './components/value-graph/value-graph.component';
import {ChartsModule} from 'ng2-charts';

const routes = [
  {path: '', component: InventoryComponent}
];

@NgModule({
  declarations: [InventoryComponent, ManaCellRendererComponent, InventoryGridComponent, SetCompletionComponent,
    GridTooltipComponent, GridNumericEditorComponent, MoveCardsDialogComponent, ValueGraphComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AgGridModule.withComponents([]),
    MatSidenavModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatProgressBarModule,
    MatExpansionModule,
    NgbPopoverModule,
    MatCheckboxModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    ChartsModule
  ]
})
export class InventoryModule {
}

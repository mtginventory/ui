export interface InventoryValue {
  eur: number;
  inventoryId: string;
  timestamp: number;
  tix: number;
  usd: number;
}

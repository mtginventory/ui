export interface InventorySummary {
  totalValue: number;
  cardCount: number;
}

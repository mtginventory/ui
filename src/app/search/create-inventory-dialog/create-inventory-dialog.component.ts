import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {InventoryService} from '../../core/http/inventory.service';

@Component({
  templateUrl: './create-inventory-dialog.component.html'
})
export class CreateInventoryDialogComponent implements OnInit {

  inventoryName = new FormControl();

  constructor(private dialogRef: MatDialogRef<CreateInventoryDialogComponent>,
              private inventoryService: InventoryService) {
  }

  ngOnInit(): void {
  }

  createInventory(): void {
    this.inventoryName.disable();
    this.inventoryService.createInventory(this.inventoryName.value).subscribe(inventory => this.dialogRef.close(inventory));
  }

  cancel(): void {
    this.dialogRef.close();
  }
}

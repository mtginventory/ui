import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {InventoryService} from '../../core/http/inventory.service';
import {Card} from '../../shared/model/card';
import {Inventory} from '../../shared/model/inventory';

@Component({
  selector: 'mtgi-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnChanges {

  disabledMessage = 'Select an inventory to add a card';

  @Input()
  card: Card | undefined;
  @Input()
  inventory: Inventory | undefined;
  quantityControl: FormControl = new FormControl({value: 0, disabled: true});

  constructor(private inventoryService: InventoryService, private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.inventory !== null) {
      this.quantityControl.enable();
    } else {
      this.quantityControl.disable();
    }
  }

  increment(): void {
    this.quantityControl.setValue(this.quantityControl.value + 1);
  }

  decrement(): void {
    if (this.quantityControl.value > 0) {
      this.quantityControl.setValue(this.quantityControl.value - 1);
    }
  }

  addCard(foil: boolean): void {
    if (this.inventory !== undefined && this.card !== undefined) {
      const quantity = this.quantityControl.value;
      this.inventoryService.addCardToInventory(this.inventory.id, {id: this.card.id}, quantity, foil)
        .subscribe(c =>
          this.matSnackBar.open(`Successfully Added ${quantity} ${c.name}`, 'Close', {
            horizontalPosition: 'right',
            verticalPosition: 'top'
          })
        );
      this.quantityControl.setValue(0);
    }
  }

  addCardTooltip(): string | boolean {
    if (this.inventory === null) {
      return this.disabledMessage;
    }
    if (this.quantityControl.value < 1) {
      return 'You must have at least 1 card added';
    }
    return false;
  }

}

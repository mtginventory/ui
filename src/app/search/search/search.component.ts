import {animate, style, transition, trigger} from '@angular/animations';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {Observable, of, Subscription} from 'rxjs';
import {debounceTime, delay, filter, mergeAll, mergeMap, switchMap, tap} from 'rxjs/operators';
import {CardService} from '../../core/http/card.service';
import {InventoryService} from '../../core/http/inventory.service';
import {Card} from '../../shared/model/card';
import {Inventory} from '../../shared/model/inventory';
import {CreateInventoryDialogComponent} from '../create-inventory-dialog/create-inventory-dialog.component';

@Component({
  selector: 'mtgi-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  animations: [
    trigger('fadeIn', [
      transition('void => *', [
        style({opacity: '0'}),
        animate('.3s', style({opacity: '1'}))
      ])
    ])
  ]
})
export class SearchComponent implements OnInit, OnDestroy {

  searchControl: FormControl = new FormControl();
  inventorySelect: FormControl = new FormControl();
  inventories$: Observable<Array<Inventory>>;
  cards: Array<Card | never> = [];
  error: string | undefined = undefined;
  private cardsSubscription: Subscription;


  constructor(private cardService: CardService, private inventoryService: InventoryService, public matDialog: MatDialog) {
    let counter = 0; // counter is required because the index from mergeMap doesn't reset with each query and the delay registration happens before any executions
    this.cardsSubscription = this.searchControl.valueChanges.pipe(
      debounceTime(400),
      tap(() => this.cards = []),
      filter(s => s.length > 1),
      switchMap(s => this.cardService.getAutocomplete(s)),
      tap(() => {
        this.cards = [];
        counter = -1;
      }),
      mergeAll(),
      mergeMap(c => of(c).pipe(delay((counter += 1) * 50)))
    ).subscribe(s => this.cards.push(s));
    this.inventories$ = inventoryService.getInventories();
  }

  ngOnDestroy(): void {
    this.cardsSubscription.unsubscribe();
  }

  ngOnInit(): void {
  }

  createInventory(): void {
    this.matDialog.open(CreateInventoryDialogComponent)
      .afterClosed().subscribe((inventory) => {
      if (inventory) {
        this.inventories$ = this.inventoryService.getInventories()
          .pipe(tap(i => this.inventorySelect.setValue(i.find(x => x.id === inventory.id))));
      }
    });
  }


}


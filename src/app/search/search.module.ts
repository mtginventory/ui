import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '../shared/material.module';
import {CardComponent} from './card/card.component';
import {CreateInventoryDialogComponent} from './create-inventory-dialog/create-inventory-dialog.component';
import {SearchComponent} from './search/search.component';


const routes: Routes = [{path: '', component: SearchComponent}];

@NgModule({
  declarations: [SearchComponent, CardComponent, CreateInventoryDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class SearchModule {
}

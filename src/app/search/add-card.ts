export interface AddCard {
  cardId: string;
  quantity: number;
  foil: boolean;
}

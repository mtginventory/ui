import {ScrollingModule} from '@angular/cdk/scrolling';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MatBadgeModule} from '@angular/material/badge';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {RouterModule, Routes} from '@angular/router';
import {CardEntryComponent} from './components/card-entry/card-entry.component';
import {CardRequestComponent} from './components/card-request/card-request.component';
import {DirectAddComponent} from './components/direct-add/direct-add.component';
import { SetsDialogComponent } from './components/sets-dialog/sets-dialog.component';


const routes: Routes = [{path: '', component: DirectAddComponent}];


@NgModule({
  declarations: [DirectAddComponent, CardEntryComponent, CardRequestComponent,SetsDialogComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatCardModule,
    MatIconModule,
    MatProgressSpinnerModule,
    ScrollingModule,
    MatButtonModule,
    MatBadgeModule,
    RouterModule.forChild(routes),
    MatDialogModule,
    MatTableModule
  ]
})
export class DirectAddModule {
}

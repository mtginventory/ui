import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ModifyCard} from '../../model/modify-card';

@Component({
  selector: 'mtgi-card-request',
  templateUrl: './card-request.component.html',
  styleUrls: ['./card-request.component.scss']
})
export class CardRequestComponent implements OnInit {

  @Input()
  request: ModifyCard | undefined;
  @Output()
  undo: EventEmitter<void> = new EventEmitter<void>();

  constructor() {
  }

  ngOnInit(): void {
  }

  emitUndo(): void {
    this.undo.emit();
  }

}

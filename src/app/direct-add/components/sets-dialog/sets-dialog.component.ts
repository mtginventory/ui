import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {ScryfallService} from '../../../core/http/scryfall.service';
import {ScryfallSet, ScryfallSets} from '../../model/scryfall-sets';

@Component({
  selector: 'mtgi-sets-dialog',
  templateUrl: './sets-dialog.component.html',
  styleUrls: ['./sets-dialog.component.scss']
})
export class SetsDialogComponent {

  sets$: Observable<ScryfallSets>;
  displayColumns = ['setName', 'releasedAt', 'icon'];

  constructor(private scryfallService: ScryfallService, private matDialogRef: MatDialogRef<SetsDialogComponent>) {
    this.sets$ = scryfallService.getSets();
  }

  rowClick(set: ScryfallSet): void {
    this.matDialogRef.close(set);
  }
}

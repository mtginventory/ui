import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ScryfallService} from '../../../core/http/scryfall.service';
import {ModifyCard} from '../../model/modify-card';
import {ScryfallSet, ScryfallSets} from '../../model/scryfall-sets';
import {SetsDialogComponent} from '../sets-dialog/sets-dialog.component';

@Component({
  selector: 'mtgi-card-entry',
  templateUrl: './card-entry.component.html',
  styleUrls: ['./card-entry.component.scss']
})
export class CardEntryComponent implements OnInit {

  selectedSet: ScryfallSet | undefined;
  @Output()
  onAddCardRequest: EventEmitter<ModifyCard> = new EventEmitter<ModifyCard>();
  private setMap: Map<string, ScryfallSet> = new Map<string, ScryfallSet>();
  inputControl: FormGroup = new FormGroup({
    set: new FormControl('', [Validators.required, this.setValidationFunction()]),
    number: new FormControl({value: undefined, disabled: true}, {updateOn: 'change'})
  });

  constructor(private scryfallService: ScryfallService, private matDialog: MatDialog) {
    scryfallService.getSets().subscribe((scryfallSets: ScryfallSets) => {
      scryfallSets.data.forEach(s => this.setMap.set(s.code, s));
    });
  }

  ngOnInit(): void {
    const setControl = this.inputControl.get('set');
    const cardNumberControl = this.inputControl.get('number');
    setControl?.statusChanges.subscribe(status => {
      if (status === 'VALID') {
        this.selectedSet = this.setMap.get(setControl.value.toLowerCase());
        if (this.selectedSet) {
          cardNumberControl?.enable();
          cardNumberControl?.setValidators([Validators.min(1), Validators.max(this.selectedSet.card_count)]);
        }
      } else {
        cardNumberControl?.disable();
        this.selectedSet = undefined;
      }
    });
  }

  private setValidationFunction(): ValidatorFn {
    const setMap = this.setMap;
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!setMap.has(control.value.toLowerCase())) {
        return {invalidSet: {value: control.value}};
      }
      // tslint:disable-next-line:no-null-keyword
      return null;
    };
  }

  submit(): void {
    if (this.inputControl.valid) {
      this.onAddCardRequest.emit(this.inputControl.value);
      this.inputControl.get('number')?.setValue(undefined);
    }
  }


  openSetSelection(): void {
    this.matDialog.open(SetsDialogComponent)
      .afterClosed().subscribe((selectedSet) => {
      this.inputControl.get('set')?.setValue(selectedSet.code);
    });
  }

}

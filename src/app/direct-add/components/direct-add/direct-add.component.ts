import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable, Subscription} from 'rxjs';
import {share} from 'rxjs/operators';
import {CardService} from '../../../core/http/card.service';
import {InventoryService} from '../../../core/http/inventory.service';
import {Card} from '../../../shared/model/card';
import {Inventory} from '../../../shared/model/inventory';
import {InventoryCard} from '../../../shared/model/inventory-card';
import {ModifyCard, ModifyType} from '../../model/modify-card';

@Component({
  selector: 'mtgi-direct-add',
  templateUrl: './direct-add.component.html',
  styleUrls: ['./direct-add.component.scss']
})
export class DirectAddComponent implements OnInit, OnDestroy {

  addRequests: Array<ModifyCard> = [];
  selectedInventory: Inventory | undefined = undefined;
  inventorySelect: FormControl = new FormControl();
  inventories$: Observable<Array<Inventory>>;
  inventorySelectionSubscription: Subscription;
  lastEnteredCard$: Observable<Card> | undefined;
  cardCount = 0;
  pullValue = 0;

  constructor(private inventoryService: InventoryService, private cardService: CardService) {
    this.inventorySelectionSubscription = this.inventorySelect.valueChanges.subscribe(i => {
      this.selectedInventory = i;
    });
    this.inventories$ = inventoryService.getInventories();
  }

  ngOnInit(): void {
  }

  handleAddCard(request: ModifyCard): void {
    request.type = ModifyType.ADD;
    this.handleModifyCardRequest(request);
  }

  handleUndo(request: ModifyCard): void {
    const requestClone: ModifyCard = JSON.parse(JSON.stringify(request));
    request.undone = true;
    delete requestClone.errorMessage;
    delete requestClone.loading;
    delete requestClone.quantity;
    delete requestClone.success;
    if (requestClone.type === ModifyType.ADD) {
      requestClone.type = ModifyType.REMOVE;
    } else {
      requestClone.type = ModifyType.ADD;
    }
    this.handleModifyCardRequest(requestClone);
  }

  ngOnDestroy(): void {
    this.inventorySelectionSubscription.unsubscribe();
  }

  private populateCardInRequest(request: ModifyCard): void {
    if (!request.name) {
      this.lastEnteredCard$ = this.cardService.getCard(request.set, request.number).pipe(share());
      this.lastEnteredCard$.subscribe(card => {
        request.name = card.name;
        request.price = card.prices?.usd;
        request.rarity = card.rarity;
        request.id = card.id;
      });
    }
  }

  private handleModifyCardRequest(request: ModifyCard): void {
    if (this.selectedInventory) {
      this.addRequests = [request, ...this.addRequests];
      this.populateCardInRequest(request);
      request.loading = true;
      let o: Observable<InventoryCard>;
      switch (request.type) {
        case ModifyType.ADD:
          o = this.inventoryService.addCardToInventory(this.selectedInventory.id, request, 1, false);
          break;
        case ModifyType.REMOVE:
          o = this.inventoryService.removeCardFromInventory(this.selectedInventory.id, request, 1, false);
          break;
        default:
          throw new Error(`Unknown modify type ${request.type}`);
      }

      o.subscribe(
        (card) => {
          request.loading = false;
          request.success = true;
          request.quantity = card.quantity;
          const price = Number(request.price);
          if (request.type === ModifyType.ADD) {
            this.cardCount += 1;
            if (!isNaN(price)) {
              this.pullValue += price;
            }
          } else {
            this.cardCount -= 1;
            if (!isNaN(price)) {
              this.pullValue -= price;
            }
          }
        }, (e) => {
          request.loading = false;
          request.success = false;
          request.errorMessage = e;
        }
      );
    }
  }

}

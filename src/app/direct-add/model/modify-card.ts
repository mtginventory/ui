export interface ModifyCard {
  set: string;
  number: number;
  rarity: string;
  name: string;
  price: string;
  loading?: boolean;
  success?: boolean;
  errorMessage?: string;
  type: ModifyType;
  id: string;
  undone: boolean;
  quantity?: number;
}

export enum ModifyType {
  ADD, REMOVE
}
